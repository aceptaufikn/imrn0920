//No 1
console.log("======No 1======");
// const golden = function goldenFunction(){
//   console.log("this is golden!!")
// }
 
// golden()
//=======
const golden = () => console.log("this is golden!!")
golden()
//No 2
console.log("======No 2======");
// const newFunction = function literal(firstName, lastName){
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function(){
//       console.log(firstName + " " + lastName)
//       return 
//     }
//   }
// }
 
// //Driver Code 
// newFunction("William", "Imoh").fullName() 
//========
const newFunction = (firstName, lastName)=>{
  return{
    firstName,
    lastName,
    fullName(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
newFunction("William", "Imoh").fullName()
//No 3
console.log("======No 3======");
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject
console.log(firstName, lastName, destination, occupation);
//No 4
console.log("======No 4======");
//Sateuacan
// const west = ["Will", "Chris", "Sam", "Holly"]
// const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// //Driver Code
// console.log(combined)
//Saatos
const west = ["Will", "Chris", "Sam", "Holly"]

const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west,...east]

console.log(combined);
//No 5
// console.log("======No 5======");
// //Saacan  
// const planet = "earth"
// const view = "glass"
// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'
 
// // Driver Code
// console.log(before) 
// Saatos
const planet = "earth"
const view = "glass"
let before = `Lorem ${view} dolor sit amet,
consectetur adipiscing elit, ${planet} do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim
ad minim veniam`

console.log(before)