import React, { Component} from 'react';
import {  StyleSheet, Text, View, Platform, Image, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './app/components/videoitem';
import data from './data.json';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import RegisteScreen from './Tugas/Tugas13/Register';
import About from './Tugas/Tugas13/AboutScreen';
import Main from './Tugas/Tugas14/components/Main';
import Counter from './Latihan/lt';
import SkillScreen from './Tugas/Tugas14/SkillScreen';
import Index from './Tugas/Tugas15/index';
import Apjs from './Tugas/Quiz3/App';


export default class App extends React.Component{
  render(){
  return (
    <View style={styles.container} >
    <Apjs />
    </View>
    // <View>
    //   <Counter />
    // </View>
    // <View style={styles.container}>
    // < />
    // </View>
    // <View style={styles.container}>
    // <Main />
    // </View>
    // <View style={styles.container}>
    // <Index />
    // </View>
    // <View style={styles.container}>
    // <Main />
    // </View>
       // <View style={styles.navBar}>
       //  <Image source={require('./assets/logo.png')} style={{width:98, height:22}} />
       //    <View style={styles.rightnav}>
       //     <TouchableOpacity>
       //      <Icon style={styles.navItem} name="search" size={25}/>
       //     </TouchableOpacity>

       //     <TouchableOpacity> 
       //      <Icon style={styles.navItem} name="account-circle" size={25}/>
       //    </TouchableOpacity>
       //    </View>
       // </View> 
       // <View style={styles.body}> 
       // <FlatList
       //    data={data.items}
       //    renderItem={(video)=><VideoItem video={video.item} />}
       //    keyExtractor={(item)=>item.id}
       //    ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
       //     />
       // </View>
       // <View style={styles.tabBar}> 
       //  <TouchableOpacity style={styles.tabItem}>
       //    <Icon name="home" size={25}/>
       //      <Text style={styles.tabTitle}>Home</Text>
       //  </TouchableOpacity>
       //  <TouchableOpacity style={styles.tabItem}>
       //    <Icon name="whatshot" size={25}/>
       //      <Text style={styles.tabTitle}>Trending</Text>
       //  </TouchableOpacity>
       //  <TouchableOpacity style={styles.tabItem}>
       //     <Icon name="subscriptions" size={25}/>
       //      <Text style={styles.tabTitle}>Subscriptions</Text>
       //  </TouchableOpacity>
       //  <TouchableOpacity style={styles.tabItem}>
       //    <Icon name="folder" size={25}/>
       //      <Text style={styles.tabTitle}>Library</Text>
       //  </TouchableOpacity>
       //  </View>
      // </View>
  );
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    height: 70,
    backgroundColor: 'white',
    elevation: 5,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  rightnav: {
    flexDirection : 'row'
  },
  navItem: {
    marginLeft: 25
  },
  body: {
    flex: 1
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTitle: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 4 
  }
});
