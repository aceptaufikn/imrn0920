import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'
import SkillItem from './components/SkillItem'
import data from './skillData.json'

export default function SkillScreen() {
  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.header}>
            <Image source={require('./assets/logo.png')} style={{width:150, height:40, marginLeft: 200}}/>
        </View>
        <View style={styles.descContainer}>
            <Image source={require('./assets/1604030119.jpg')} style={{width:40, height: 60, marginRight:10, marginLeft: 10}}/>
            <View style={styles.Desc}>
                <Text numberOfLines={2} style={styles.Titletxt}>Hai,</Text>
                <Text style={styles.nametxt}>Acep Taufik Nurojab</Text>
            </View>
        </View>
        <View style={styles.box}>
            <Text style={styles.intitle}>Skill</Text>
            <View style={styles.Bar} />
        <View style={styles.skillList}>
                <TouchableOpacity style={styles.btn}>
                    <Text style={styles.btntxt}>Library / Framework</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btn2}>
                    <Text style={styles.btntxt}>Bahasa Pemrograman</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btn3}>
                    <Text style={styles.btntxt}>Teknologi</Text>
                </TouchableOpacity>
            </View>
        </View>
        <View style={styles.body}>
            <FlatList 
                data={data.items}
                renderItem ={(skill)=><SkillItem skill={skill.item} />}
                keyExtractor={(item)=>item.id}
                ItemSeparatorComponent={()=> <View style={{height:5,backgroundColor:'#E5E5E5'}}/>}
            />

        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        height: 40,
        width: 100
    },
    header: {
        height: 55,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    navItem: {
        marginLeft: 25
    },
    body: {
        flex: 1,
        padding: 5,
        backgroundColor: '#B4E9FF',
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 8
    },
    descContainer: {
        flexDirection: 'row',
        paddingTop: 5,
        marginTop: 10
    },
    Titletxt: {
        fontSize: 12,
        color: '#000000'
    },
    Desc: {
        paddingHorizontal: 1,
    },
    nametxt: {
        fontSize: 16,
        paddingTop: 4,
        color: '#003366'
    },
    box: {
        borderColor: '#B4E9FF',
        borderRadius: 10,
        borderBottomColor: '#000',
        padding: 5,
        backgroundColor: "white",
        marginBottom: 9,
        marginLeft : 4
    },
    intitle: {
        fontSize: 36,
        color: '#003366'
    },
    skillList: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'space-around',
    }, 
    btn: {
        alignItems: 'center',
        backgroundColor: '#B4E9FF',
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 1,
        marginBottom: 1,
        marginTop: 1,
        marginRight: 10,
        width: 130,
        height: 37
    },
    btn2: {
        alignItems: 'center',
        backgroundColor: '#B4E9FF',
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 1,
        marginBottom: 1,
        marginTop: 1,
        marginRight: 10,
        width: 130,
        height: 37
    },
    btn3: {
        alignItems: 'center',
        backgroundColor: '#B4E9FF',
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 1,
        marginBottom: 1,
        marginTop: 1,
        width: 80,
        height: 37,
    },
    btntxt: {
        color: '#003366',
        textAlign: 'center',
        fontSize: 11,
        fontWeight: 'bold',
        justifyContent: 'center'
    },
    Bar: {
    backgroundColor: '#B4E9FF',
    height: 5,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderRadius: 5
}
});