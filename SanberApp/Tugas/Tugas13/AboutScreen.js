import React, { Component} from 'react';
import {  StyleSheet, Text, View, Platform, Image, TouchableOpacity, TextInput, FlatList, SafeAreaView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class About extends Component{
	render(){
		return(
			<View style={styles.container}>
                <Text style={styles.header}>Tentang Saya</Text>
                <Icon style={styles.poto} name="account-circle" size={140}/>
                <Text style={styles.name}>Acep Taufik Nurojab</Text>
                <Text style={styles.status}>React Native Developer</Text>
                <View style={styles.box}>
                    <Text style={{fontSize: 16}}>Portofolio</Text>
                    <View style={styles.separator}/>
                    <View style={styles.itemH}>
                        <View style={styles.itempf}>
                        	<Image source={require("./image/gitlab1.png")} style={styles.logo}/>
                            <Text style={styles.logoText}>@aceptaufikn</Text>
                        </View>
                        <View style={styles.itempf}>
                        	<Image source={require("./image/github.png")} style={styles.logo}/>
                            <Text style={styles.logoText}>@aceptaufikn</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.box}>
                    <Text style={{fontSize: 16}}>Hubungi Saya</Text>
                    <View style={styles.separator}/>
                    <View style={styles.itemV}>
                        <View style={styles.itemhs}>
                        	<Image source={require("./image/fb.png")} style={styles.logo}/>
                            <Text style={styles.logoText}>Acep Taufik Nurojab</Text>
                        </View>
                        <View style={styles.itemhs}>
                        	<Image source={require("./image/ig.png")} style={styles.logo}/>
                            <Text style={styles.logoText}>@acep.tn</Text>
                        </View>
                        <View style={styles.itemhs}>
                        	<Image source={require("./image/twr.png")} style={styles.logo}/>
                            <Text style={styles.logoText}>@TnAcep</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const colors = {
    lightBlue: "#3EC6FF",
    darkBlue: "#003366",
    grayBackground: "#EFEFEF"
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    header: {
        color: colors.darkBlue, 
        fontSize: 30, 
        fontWeight: 'bold',
        marginTop: 44,
        marginBottom: 12
    },
    porto: {
    	fontSize: 16,
    	color: '#003366'
    },
    poto: {
      marginTop: -10,
        marginBottom: -10
    },
    name: {
        fontSize: 24,
        fontWeight: "bold",
        color: '#003366',
        marginTop: 10,
        marginBottom: 4
    },
    status: {
        fontSize: 16,
        color: colors.lightBlue,
        marginBottom: 12
    },
    box: {
        width: "90%",
        padding: 8,
        borderRadius: 16,
        backgroundColor: colors.grayBackground,
        alignItems: "flex-start",
        marginBottom: 12
    },
    separator: {
        width: "100%",
        height: 0,
        borderWidth: 0.5,
        borderColor: colors.darkBlue,
        marginVertical: 4
    },
    itemH: {
        marginVertical: 10,
        flexDirection: 'row',
        width: "100%",
        justifyContent: 'space-around'
    },
    itempf: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        width: 35,
        height: 35,
    },
    logoText: {
        fontSize: 14,
        color: colors.darkBlue,
        fontWeight: "bold",
        marginTop: 6,
        marginHorizontal: 14
    },
    itemV: {
        width: "100%",
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    itemhs: {
        width: 140,
        flexDirection: 'row',
        margin: 5
    }
})