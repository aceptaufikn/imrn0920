import React, { Component} from 'react';
import {  StyleSheet, Text, View, Platform, Image, TouchableOpacity, TextInput, FlatList, SafeAreaView } from 'react-native';

export default class RegisteScreen extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.Header}>
          <Image style={styles.logo} source={require("./image/logo.png")}
          />
        </View>

        <View style={styles.Title}>
          <Text style={styles.Titletxt}>Register</Text>
        </View>

        <View style={styles.formLogin}>
          <Text style={styles.txtForm}>Username</Text>
          <TextInput style={styles.inputtxt} />

          <Text style={styles.txtForm}>Email</Text>
          <TextInput style={styles.inputtxt} />

          <Text style={styles.txtForm}>Password</Text>
          <TextInput style={styles.inputtxt} />

          <Text style={styles.txtForm}>Ulangi Password</Text>
          <TextInput style={styles.inputtxt} />

          <TouchableOpacity style={styles.Button2}>
            <Text style={styles.txtBtn}>Daftar ?</Text>
          </TouchableOpacity>

          <Text style={styles.txtAtau}>atau</Text>

          <TouchableOpacity style={styles.Button1}>
            <Text style={styles.txtBtn}>Masuk</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    height: "100%",
  },
  Header: {
    marginTop: 60,
    alignItems: "center",
  },
  logo: {
    height: 100,
  },
  Title: {
    marginTop: 50,

    alignItems: "center",
  },
  Titletxt: {
    color: "#003366",
    fontSize: 25,
    alignItems: "center",
  },
  formLogin: {
    padding: 30,
    marginTop: -10,
  },
  txtForm: {
    fontSize: 16,
    color: "#003366",
    marginTop: 3,
  },
  inputtxt: {
    borderWidth: 1,
    borderColor: '#003366',
    marginTop: 2,
    height: 30,
  },
  Button2: {
    backgroundColor: "#3EC6FF",
    borderRadius: 15,
    height: 35,
    marginTop: 20,
    width: 120,
    alignSelf: "center",
  },
  Button1: {
    backgroundColor: "#003366",
    borderRadius: 15,
    marginTop: 10,
    height: 35,
    width: 120,
    alignSelf: "center",
  },
  txtBtn: {
    color: "white",
    fontSize: 20,
    textAlign: "center",
    paddingTop:3,
  },
  txtAtau: {
    fontSize: 18,
    alignSelf: "center",
    marginTop: 10,
    color: "#3EC6FF",
  },
});