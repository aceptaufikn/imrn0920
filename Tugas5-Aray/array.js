//NO 1
console.log("=====No 1=====")
function range(startNum, finishNum){
	let array=[];
	if (startNum<=finishNum) {
		for(let i=startNum; i<=finishNum; i++){
			array.push(i);
		}
	}if (startNum>=finishNum) {
		for(let i=startNum; i>=finishNum; i--){
			array.push(i);
		}
	}else{	
	}
	return array;
}
console.log(range(1,10));
//NO 2
console.log("=====No 2=====")
function rangeWithStep(startNum, finishNum, step) {
    var array = []
    if (startNum <= finishNum) {
        for (let i = startNum; i <= finishNum; i += step) {
            array.push(i);
        }
        return array;
    } else {
        for (let i = startNum; i >= finishNum; i -= step) {
            array.push(i);
        }
        return array;
    }
}

console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4)) 
//NO 3
console.log("=====No 3=====")
function sum(startNum, finishNum, step = 1) {
  const rangeArray = rangeWithStep(startNum, finishNum, step);
  return rangeArray.reduce((acc, cur) => acc + cur, 1);
}
console.log(sum(A,F)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0
//NO 4
console.log("=====No 4=====")
function dataHandling(input){
  var data='';
  for (var n=0; n<input.length; n++ ){
    data += 'Nomor ID: ' + input[n][0] + '\n' + 
                'Nama Lengkap: ' + input[n][1] + '\n' + 
                'TTL: ' + input[n].slice(1,3).join(' ') + '\n' + 
                'Hobi: ' + input[n][4]+ '\n' + '\n';
  }
  return data;
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]

console.log(dataHandling(input));
//NO 5
console.log("=====No 5=====")
function balikKata(kata){
   return kata.split("").reverse().join("");
}

console.log(balikKata("Kasur Rusak")) 
console.log(balikKata("SanberCode")) 
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar")) 
console.log(balikKata("I am Sanbers"))  

